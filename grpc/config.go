package grpc

import (
	"io/ioutil"
	"gitlab.com/mage-repo/template-utils/errors"
	"gopkg.in/yaml.v2"
	"fmt"
)

type Configs struct {
	Config []*Config `yaml:"client"`
}

type Config struct {
	Url  string `yaml:"url"`
	Port string `yaml:"port"`
	Name string `yaml:"name"`
}




func Read(path string) (*Configs, error) {
	var configs = &Configs{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.NewInvalidArgumentsError(fmt.Sprintf("%v",err))
	}

	err = yaml.Unmarshal(data, configs)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Sprintf("%v",err))
	}
	return configs, nil
}
