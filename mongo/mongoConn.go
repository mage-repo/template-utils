package utils

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db *mongo.Client
var MGdb *mongo.Collection
var mongoCtx context.Context

func Setup(conf *Config) {
	// Initialize MongoDb client
	theUrl := fmt.Sprintf("%s", conf.Service.URL) //Should be of the format mongodb://ipaddress:port
	fmt.Println("Connecting to MongoDB...")
	mongoCtx = context.Background()
	db, err := mongo.Connect(mongoCtx, options.Client().ApplyURI(theUrl))
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping(mongoCtx, nil)
	if err != nil {
		log.Fatalf("Could not connect to MongoDB: %v\n", err)
	} else {
		fmt.Println("Connected to Mongodb")
	}
	dataBase := conf.Service.DBName
	collectName := conf.Service.CollName
	MGdb = db.Database(dataBase).Collection(collectName)
}

func GetDB() *mongo.Collection {
	return MGdb
}
