///Implemented
package utils

import (
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

var FieldDataTypeMap = map[string]string{
	"integer": "int32",
	"string": "string",
	"boolean": "bool",
}

// Port and dbname add here


type Config struct {
	Service struct {
		UUID     string `yaml:"uuid"`
		Name     string `yaml:"name"`
		Type     string `yaml:"type"`
		URL      string `yaml:"url"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		DBName string `yaml:"dbName"`
		CollName string `yaml:"collName"`
	} `yaml:"service"`
	Methods []Method `yaml:"methods"`
}

type Method struct {
	Name           string          `yaml:"name"`
	QueryType      string          `yaml:"queryType"`
	Requests 	   []Request 	   `yaml:"request"`
	Response       string        `yaml:"response"`
	JSONquery      string  		`yaml:"queryJSON"`
}

type Request struct {
	Name           string  `yaml:"name"`
	variableType  string  `yaml:"type"`
}


func GetConfig(configPath string) *Config {
	var config Config
	yamlFile, _ := ioutil.ReadFile(configPath)
	_ = yaml.Unmarshal(yamlFile, &config)
	return &config
}

func WriteConfig(config *Config, path string) {
	f, _ := os.Create(path)
	yamlData, _ := yaml.Marshal(config.Service)
	f.Write(yamlData)
}