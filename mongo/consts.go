// Implemented
package utils

import (
	"fmt"
	"os"
)

var GOPATH = os.Getenv("GOPATH")
var BasePath string
var ServiceID string
var NodeID string
var GenPath string
var (
	MainGenPath         string
	HandlersGenPath     string
	ConfigYamlGenPath   string
	Proto               string
	HandlersSuffix      string
	HandlerTemplatePath string
	MainTemplatePath    string
)

func SetBasePath(bp string) {
	BasePath = bp
}

func SetServiceID(s string) {
	ServiceID = s
}
func SetNodeID(n string) {
	NodeID = n
}

func InitPaths() {
	GenPath = fmt.Sprintf("%s/src/%s/%s/app/%s", GOPATH, BasePath, ServiceID, NodeID)
	MainGenPath = fmt.Sprintf("%s/main.go", GenPath)
	HandlersGenPath = fmt.Sprintf("%s/handlers/handlers.go", GenPath)
	ConfigYamlGenPath = fmt.Sprintf("%s/config/config.yaml", GenPath)
	Proto = fmt.Sprintf(`%s/%s/proto_gen`, BasePath, ServiceID)
	HandlersSuffix = fmt.Sprintf("/app/%s/handlers", NodeID)
}

type TemplateConfig struct {
	FileName string
	Path     string
}

var TemplateConfigs = []TemplateConfig{
	{
		FileName: "main.tgo",
		Path:     fmt.Sprintf("%s/src/gitlab.com/mage-io/mongodb-service/templates/main.tgo", GOPATH),
		
	},
	{
		FileName: "handlers.tgo",
		Path:     fmt.Sprintf("%s/src/gitlab.com/mage-io/mongodb-service/templates/handlers/handlers.tgo", GOPATH),
	},
}