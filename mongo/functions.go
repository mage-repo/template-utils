package utils

import (
	//"fmt"
	"strings"

	"text/template"
	"os"
)


func CreateTemplateCache() map[string]*template.Template {
	tCache := map[string]*template.Template{}

	funcMap := map[string]interface{}{
		"toTitle" : strings.Title,
	}

	for _, temp := range TemplateConfigs {
		tCache[strings.Split(temp.FileName, ".")[0]]= template.Must(template.New(temp.FileName).Funcs(funcMap).ParseFiles(temp.Path))
	}

	return tCache
}


func Execute(fileName string, vars TemplateVariables, t *template.Template, genPath string) {
	f, _ := os.Create(genPath)
	err := t.Execute(f, vars)
	if err != nil {
		panic(err)
	}
}
