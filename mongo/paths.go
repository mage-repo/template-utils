//Implemented
package utils

import (
	"fmt"
	"strings"
)

type TemplateVariables struct {
	Imports     []string
	Methods     []Method
	ServiceName string
}

type TempGenData struct {
	TempVars TemplateVariables
	GenPath  string
}

func GetTemplateDataMap(conf *Config) map[string]TempGenData {
	fileToImports := make(map[string]TempGenData)

	//main
	fileToImports["main"] = TempGenData{
		TempVars: TemplateVariables{
			Imports: []string{
				`log`,
				`fmt`,
				`net`,
				`google.golang.org/grpc`,

				Proto,
				fmt.Sprintf(`%s/%s%s`, BasePath, ServiceID, HandlersSuffix),
			},
			Methods:     conf.Methods,
			ServiceName: strings.Title(conf.Service.Name),
		},
		GenPath: MainGenPath,
		
	}

	//handlers
	fileToImports["handlers"] = TempGenData{
		TempVars: TemplateVariables{
			Imports: []string{
				`fmt`,
				`context`,
				`strings`,
				`encoding/json`,
				`go.mongodb.org/mongo-driver/mongo`,
				 Proto,
			},
			Methods:     conf.Methods,
			ServiceName: strings.Title(conf.Service.Name),
		},
		GenPath: HandlersGenPath,
	}

	return fileToImports
}
