package server

import (
	"encoding/json"
	"io/ioutil"

	logging "gitlab.com/mage-repo/template-utils/logger"
	"go.uber.org/zap"
	"google.golang.org/grpc/keepalive"
)

func GetServerParams(path string) *keepalive.ServerParameters {

	var logger *zap.Logger = logging.GetLogger()
	rawJSON, err := ioutil.ReadFile(path)
	if err != nil {
		rawJSON = DEFAUL_KEEPALIVE_CONF
		logger.Error("cannot read keepalive config, using default configs")
	}
	var keepaliveConfig keepalive.ServerParameters
	if err := json.Unmarshal(rawJSON, &keepaliveConfig); err != nil {
		logger.Error("cannot unmarshal config into keepaliveConfig")
		panic(err)
	}
	return &keepaliveConfig
}

func GetInterceptorParams(path string) *InterceptorConfig {

	var logger *zap.Logger = logging.GetLogger()
	rawJSON, err := ioutil.ReadFile(path)
	if err != nil {
		rawJSON = DEFAULT_INTERCEPTOR_CONF
		logger.Error("cannot read interceptor config, using default configs")
	}
	var interceptorConf InterceptorConfig
	if err := json.Unmarshal(rawJSON, &interceptorConf); err != nil {
		logger.Error("cannot unmarshal config into interceptorConf")
		panic(err)
	}
	return &interceptorConf
}
