package server

const (
	METHOD_NAME  = "grpc.method"
	SERVICE_NAME = "grpc.service"
)

var (
	DEFAUL_KEEPALIVE_CONF    = []byte(`{"MaxConnectionIdle": 1, "Time": 2, "Timeout": 5}`)
	DEFAULT_INTERCEPTOR_CONF = []byte(`{ "enabled": { "middleware_grpc_ctx": true, "middleware_zap_logger": true, "middleware_zap_payload_logger": true, "middleware_grpc_recovery": true}}`)
)
