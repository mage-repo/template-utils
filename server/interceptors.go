package server

import (
	"context"
	"fmt"
	logging "github.com/grpc-ecosystem/go-grpc-middleware/logging"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"gitlab.com/mage-repo/template-utils/errors"
	logger "gitlab.com/mage-repo/template-utils/logger"
	"google.golang.org/grpc"
	"strings"
)

func UnaryInterceptor() grpc.UnaryServerInterceptor {

	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {

		fullMethod := strings.Split(string(info.FullMethod), "/")
		ctx = context.WithValue(ctx, SERVICE_NAME, fullMethod[1])
		ctx = context.WithValue(ctx, METHOD_NAME, fullMethod[2])
		h, err := handler(ctx, req)
		return h, err
	}
}

func ServerPayloadLoggingDecider() logging.ServerPayloadLoggingDecider {

	return func(ctx context.Context, fullMethodName string, servingObject interface{}) bool {
		return true
	}
}

func ErrHandling(p interface{}) error {

	return errors.NewInternalError(fmt.Sprintf("%v", p))
}

func GetUnaryServerInterceptors(interceptorParams *InterceptorConfig) []grpc.UnaryServerInterceptor {
	recoveryopts := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(ErrHandling),
	}
	interceptorArr := []grpc.UnaryServerInterceptor{}
	if interceptorParams.Enabled.Middleware_grpc_ctx {
		interceptorArr = append(interceptorArr, UnaryInterceptor())
	}
	if interceptorParams.Enabled.Middleware_zap_logger {
		interceptorArr = append(interceptorArr, grpc_zap.UnaryServerInterceptor(logger.GetLogger()))
	}
	if interceptorParams.Enabled.Middleware_zap_payload_logger {
		interceptorArr = append(interceptorArr, grpc_zap.PayloadUnaryServerInterceptor(logger.GetLogger(), ServerPayloadLoggingDecider()))
	}
	if interceptorParams.Enabled.Middleware_grpc_recovery {
		interceptorArr = append(interceptorArr, grpc_recovery.UnaryServerInterceptor(recoveryopts...))
	}

	return interceptorArr
}
