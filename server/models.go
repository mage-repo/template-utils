package server

type InterceptorConfig struct {
	Enabled Enabled `json:"enabled"`
}

type Enabled struct {
	Middleware_grpc_ctx           bool `json:"middleware_grpc_ctx"`
	Middleware_zap_logger         bool `json:"middleware_zap_logger"`
	Middleware_zap_payload_logger bool `json:"middleware_zap_payload_logger"`
	Middleware_grpc_recovery      bool `json:"middleware_grpc_recovery"`
}
