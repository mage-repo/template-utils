package server

import (
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

type GrpcServerBuilder struct {
	options []grpc.ServerOption
}

func (sb *GrpcServerBuilder) AddOption(o grpc.ServerOption) {
	sb.options = append(sb.options, o)
}

func (sb *GrpcServerBuilder) SetServerParameters(serverParams *keepalive.ServerParameters) {
	keepAlive := grpc.KeepaliveParams(*serverParams)
	sb.AddOption(keepAlive)
}

func (sb *GrpcServerBuilder) SetUnaryInterceptors(interceptors []grpc.UnaryServerInterceptor) {
	chain := grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(interceptors...))
	sb.AddOption(chain)
}

func (sb *GrpcServerBuilder) Build() *grpc.Server {
	srv := grpc.NewServer(sb.options...)
	return srv
}

func addInterceptors(s *GrpcServerBuilder, interceptorParams *InterceptorConfig) {
	s.SetUnaryInterceptors(GetUnaryServerInterceptors(interceptorParams))
}

func Init(keepaliveConfPath string, interceptorConfPath string) *grpc.Server {

	builder := GrpcServerBuilder{}
	serverParams := GetServerParams(keepaliveConfPath)
	builder.SetServerParameters(serverParams)
	interceptorParams := GetInterceptorParams(interceptorConfPath)
	addInterceptors(&builder, interceptorParams)
	s := builder.Build()
	return s

}
