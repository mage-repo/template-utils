package logger

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/mage-repo/template-utils/errors"

	"go.uber.org/zap"
)

var logger *zap.Logger

func GetLogger() *zap.Logger {
	return logger
}

func getConfig(path string) (zap.Config, error) {

	var cfg zap.Config
	rawJSON, _ := ioutil.ReadFile(path)
	if err := json.Unmarshal(rawJSON, &cfg); err != nil {
		return cfg, errors.NewInvalidArgumentsError(fmt.Sprintf("%s", err))
	}
	return cfg, nil

}

func Init(configPath string) {

	usedDefaultLogConfig := false
	cfg, err := getConfig(configPath)
	if err != nil {
		if err := json.Unmarshal(DEFAULT_CONFIG, &cfg); err != nil {
			panic(err)
		}
		usedDefaultLogConfig = true
	}
	logger, err = cfg.Build()
	if err != nil {
		panic(err)
	}
	if usedDefaultLogConfig {
		logger.Error("cannot read logger config, using default configs")
	}
	defer logger.Sync()
	logger.Info("logger construction succeeded")
}

func retrieveLogParams(ctx context.Context) []zap.Field {

	zapParams := []zap.Field{}
	for _, ele := range LoggerParams {
		if val := ctx.Value(ele); val != nil {
			zapParams = append(zapParams, zap.String(ele, val.(string)))
		}

	}

	return zapParams

}

func LogInfo(ctx context.Context, str string) {

	arr := retrieveLogParams(ctx)
	logger.Info(str, arr...)
}

func LogWarn(ctx context.Context, str string) {

	arr := retrieveLogParams(ctx)
	logger.Warn(str, arr...)
}

func LogErr(ctx context.Context, str string) {

	arr := retrieveLogParams(ctx)
	logger.Error(str, arr...)
}
