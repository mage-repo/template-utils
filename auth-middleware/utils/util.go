package utils

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Token struct {
		TokenSource    string `yaml:"token_source"`
		TokenFieldPath string `yaml:"token_field_path"`
		TokenType      string `yaml:"token_type"`
		PublicKey      string `yaml:"public_key"`
		ClientID       string `yaml:"client_id"`
	} `yaml:"token"`
}

func Read(path string) (*Config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := &Config{}

	err = yaml.Unmarshal(data, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
