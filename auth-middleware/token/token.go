package token

import (
	"context"
	"crypto/rsa"
	b64 "encoding/base64"
	"fmt"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/mage-repo/template-utils/auth-middleware/utils"
	google "google.golang.org/api/idtoken"
)

type Validator interface {
	Validate(token string) error
}

type GoogleValidator struct {
	clientId string
}
type RS256Validator struct {
	publicKey *rsa.PublicKey
}

type FacebookValidator struct {
}

func InitValidator(config *utils.Config) (Validator, error) {
	tokenType := config.Token.TokenType

	if tokenType == "RS256" {
		key, err := b64.StdEncoding.DecodeString(config.Token.PublicKey)
		publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
		if err != nil {
			return nil, err
		}
		return &RS256Validator{
			publicKey: publicKey,
		}, nil
	}

	if tokenType == "GoogleOAuth" {
		return &GoogleValidator{
			clientId: config.Token.ClientID,
		}, nil
	}
	if tokenType == "FacebookOAuth" {
		return &FacebookValidator{}, nil
	}
	return nil, fmt.Errorf("Invalid token type %s", tokenType)
}

func (v *GoogleValidator) Validate(token string) error {
	ctx := context.Background()
	_, err := google.Validate(ctx, token, v.clientId)
	return err
}

func (v *RS256Validator) Validate(token string) error {

	_, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}
		return v.publicKey, nil
	})

	if err != nil {
		return fmt.Errorf("Invalid Token")
	}
	return err
}

func (v *FacebookValidator) Validate(token string) error {
	return nil
}
