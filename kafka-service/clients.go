package kafka

import (
	"context"
	"fmt"
	"io/ioutil"

	"gitlab.com/mage-repo/template-utils/errors"
	"gitlab.com/mage-repo/template-utils/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"gopkg.in/yaml.v2"
)

type Configs struct {
	Config []*Config `yaml:"client"`
}

type Config struct {
	Url  string `yaml:"url"`
	Port string `yaml:"port"`
	Name string `yaml:"name"`
}

var Connections = map[string]*grpc.ClientConn{}

func GetConnections(ctx context.Context, configs *Configs) {
	for _, c := range configs.Config {
		conn, err := grpc.Dial(c.Url+c.Port, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			logger.LogErr(ctx, fmt.Sprintf("\nCould not connect to %s at %s :%s\n", c.Name, c.Url+c.Port, err))
			continue
		}
		Connections[c.Name] = conn
	}
}
func Read(path string) (*Configs, error) {
	var configs = &Configs{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.NewInvalidArgumentsError(fmt.Sprintf("%v", err))
	}

	err = yaml.Unmarshal(data, configs)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Sprintf("%v", err))
	}
	return configs, nil
}
