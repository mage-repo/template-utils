package kafka

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"io/ioutil"

	"github.com/segmentio/kafka-go"
	"gitlab.com/mage-repo/template-utils/errors"
	"gitlab.com/mage-repo/template-utils/logger"
	"gopkg.in/yaml.v2"
)

type Destination struct {
	Service string
	Method  string
}

type MessageWithDestination struct {
	Initiator   string
	Destination Destination
}

type ServiceConfig struct {
	Url      string `yaml:"url"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Group    string `yaml:"group"`
	Topic    string `yaml:"Topic"`
}

func ReadServiceConfig(path string) (*ServiceConfig, error) {
	var configs = &ServiceConfig{}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.NewInvalidArgumentsError(fmt.Sprintf("%v", err))
	}

	err = yaml.Unmarshal(data, configs)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Sprintf("%v", err))
	}
	return configs, nil
}

func DecodeMessage(ctx context.Context, m kafka.Message) MessageWithDestination {
	var messageWithDestination MessageWithDestination
	buffer := bytes.NewBuffer(m.Value)
	dec := gob.NewDecoder(buffer)
	err := dec.Decode(&messageWithDestination)
	if err != nil {
		logger.LogErr(ctx, fmt.Sprintf("Error while decoding: %s", err))
	}
	return messageWithDestination
}

func GetKafkaReader(brokers []string, groupID string, topic string, partition int) *kafka.Reader {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   brokers,
		GroupID:   groupID,
		Topic:     topic,
		Partition: partition,
		MinBytes:  10e3, // 10KB
		MaxBytes:  10e6, // 10MB
	})

	return r
}
