package kafka

import (
	"context"
	"fmt"
	"log"

	"github.com/segmentio/kafka-go"
	"gitlab.com/mage-repo/template-utils/logger"
)

func Producer(ctx context.Context, url string, topic string, partition int, messages ...kafka.Message) error {

	w := &kafka.Writer{
		Addr:     kafka.TCP(url),
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
		Async:    true,
	}

	err := w.WriteMessages(context.Background(), messages...)

	if err != nil {
		logger.LogErr(ctx, fmt.Sprintf("failed to write messages:%s", err))
		return err
	}

	log.Printf("Success...")

	if err != nil {
		logger.LogErr(ctx, fmt.Sprintf("failed to write messages:%s", err))

		return err
	}

	if err := w.Close(); err != nil {
		logger.LogErr(ctx, fmt.Sprintf("failed to close writer:%s", err))
		return err
	}

	return nil
}
