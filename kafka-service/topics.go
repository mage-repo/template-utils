package kafka

import (
	"fmt"
	"net"
	"strconv"

	kafka "github.com/segmentio/kafka-go"
)

func CreateTopic(url string, topic string, partition int) error {
	conn, err := kafka.Dial("tcp", url)
	if err != nil {
		return err
	}
	defer conn.Close()
	controller, err := conn.Controller()
	if err != nil {
		return err
	}
	var controllerConn *kafka.Conn

	controllerConn, err = kafka.Dial(
		"tcp",
		net.JoinHostPort(
			controller.Host,
			strconv.Itoa(controller.Port),
		),
	)
	if err != nil {
		return err
	}
	defer controllerConn.Close()
	topicConfigs := []kafka.TopicConfig{
		{
			Topic:             topic,
			NumPartitions:     partition,
			ReplicationFactor: 1,
		},
	}
	err = controllerConn.CreateTopics(topicConfigs...)
	fmt.Printf("Created topic")
	if err != nil {
		return err
	}
	return nil
}
