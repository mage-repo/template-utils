package s3

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"sync"
)

var (
	sess *session.Session
	once sync.Once
	err  error
)

func connectAWS() *session.Session {
	// Create a New  AWS Session
	conf := Config.Service
	sess, err := session.NewSession(
		&aws.Config{
			Region:      aws.String(conf.Region),
			Credentials: credentials.NewStaticCredentials(conf.AccessKeyID, conf.SecretAccessKey, ""),
		},
	)

	if err != nil {
		return nil
	}

	return sess
}

func GetSession() *session.Session {
	once.Do(func() {
		sess = connectAWS()
	})
	return sess
}
