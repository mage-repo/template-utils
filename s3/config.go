package s3

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var Config *StorageConfig

type StorageConfig struct {
	Service *ServiceConfig `yaml:"service"`
}
type ServiceConfig struct {
	Platform        string `yaml:"platform"`
	StorageType     string `yaml:"storage_type"`
	AccessKeyID     string `yaml:"access_key_id"`
	SecretAccessKey string `yaml:"secret_access_key"`
	Region          string `yaml:"region"`
}

func Read(path string) (*StorageConfig, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := &StorageConfig{}
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return nil, err
	}
	Config = config
	return Config, nil
}
