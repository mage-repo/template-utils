package grafana

import (
	"github.com/prometheus/client_golang/prometheus"
)

var HttpReqs = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "How many HTTP requests processed, partitioned by status code and HTTP method.",
	},
	[]string{"Service", "method"},
)

var HttpResps = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_response_total",
		Help: "How many HTTP responses sent",
	},
	[]string{"Service", "method"},
)

var HttpErrs = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_error_total",
		Help: "How many HTTP erros got",
	},
	[]string{"Service", "method"},
)

var HttpErrsCode = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_error_code",
		Help: "Error codes of all",
	},
	[]string{"Service", "method"},
)
