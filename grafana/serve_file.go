package grafana

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func Init() {
	prometheus.Register(HttpReqs)
	prometheus.Register(HttpResps)
	prometheus.Register(HttpErrs)
	prometheus.Register(HttpErrsCode)
	go func() {
		//http.Handle("/metrics", promhttp.Handler())
		http.Handle("/metrics", promhttp.Handler()) //For(registry, promhttp.HandlerOpts{})
		http.ListenAndServe(":3000", nil)
	}()
}
