package utils

import (
	"fmt"
	"gitlab.com/mage-repo/template-utils/errors"
	"google.golang.org/grpc"
)

var connMap map[string]*grpc.ClientConn

func SetUp(config *Config) error {

	connMap = make(map[string]*grpc.ClientConn)
	for _, method := range config.Methods {
		for _, connection := range method.Connections {

			var conn *grpc.ClientConn
			conn, err := grpc.Dial(fmt.Sprintf("%s:8080", connection.Service), grpc.WithInsecure())
			if err != nil {
				return errors.NewInternalError(fmt.Sprintf("%v", err))
			}
			if _, ok := connMap[connection.Service]; !ok {
				connMap[connection.Service] = conn
			}
		}

	}
	return nil

}

// GetDB helps you to get a connection
func GetConnMap() map[string]*grpc.ClientConn {
	return connMap
}
