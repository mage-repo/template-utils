package utils

import (
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

// Port and dbname add here

type Config struct {
	Service struct {
		UUID     string `yaml:"uuid"`
		Name     string `yaml:"name"`
		Type     string `yaml:"type"`
	} `yaml:"service"`
	Methods []Method `yaml:"methods"`
}

type Method struct {
	Name           string          `yaml:"name"`
	Connections    []Connection 	`yaml:"service_connections"`
	
}

type Connection struct {

	Service             string    `yaml:"service"`
	Type                string    `yaml:"type"`
	MethodName          string    `yaml:"method"`
	Is_request_string   bool      `yaml:"is_request_string"`
	Is_response_string  bool      `yaml:"is_response_string"`
	Schema              string    `yaml:"schema"`
	Has_where_clause    bool      `yaml:"has_where_clause"`
}

func GetConfig(configPath string) *Config {
	var config Config
	yamlFile, _ := ioutil.ReadFile(configPath)
	_ = yaml.Unmarshal(yamlFile, &config)
	return &config
}

func WriteConfig(config *Config, path string) {
	f, _ := os.Create(path)
	yamlData, _ := yaml.Marshal(config.Service)
	f.Write(yamlData)
}
