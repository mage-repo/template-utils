package async

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestAsyncOrchestrator_Send(t *testing.T) {
	type input struct {
		id      string
		message string
	}
	tests := []struct {
		name   string
		inputs []input

		expectedBulkMessageCalls map[string]int64
		expectedSendCalls        map[string]int64
	}{
		{
			name: "basic test",
			inputs: []input{
				{id: "no-bulk-id-1", message: "m1"},
				{id: "no-bulk-id-1", message: "m2"},
				{id: "no-bulk-id-2", message: "m3"},
				{id: "no-bulk-id-2", message: "m4"},
				{id: "bulk-id-1", message: "m5"},
				{id: "bulk-id-1", message: "m6"},
				{id: "bulk-id-2", message: "m7"},
				{id: "bulk-id-2", message: "m8"},
				{id: "bulk-id-random", message: "m9"},
			},
			expectedSendCalls: map[string]int64{
				"no-bulk-id-1": 2,
				"no-bulk-id-2": 2,
			},
			expectedBulkMessageCalls: map[string]int64{
				"bulk-id-1": 1,
				"bulk-id-2": 1,
			},
		},
	}

	for _, tt := range tests {
		test := tt
		t.Run(test.name, func(t *testing.T) {
			orch := getMockOrchestrator()
			orch.bulkAsyncReader()
			go orch.asyncReader()
			time.Sleep(1 * time.Second)
			for _, inp := range test.inputs {
				orch.Send(context.Background(), inp.id, map[string]string{"random": inp.message})
			}
			time.Sleep(2 * time.Second)
			for id, count := range test.expectedSendCalls {
				assert.Equal(t, count, orch.messageHandlerMap[id].(*mockMessageHandler).sendNumCalled)
			}
			for id, count := range test.expectedBulkMessageCalls {
				assert.Equal(t, count, orch.messageHandlerMap[id].(*mockMessageHandler).sendBulkNumCalled, "id: %v", id)
			}
			for id, _ := range test.expectedBulkMessageCalls {
				assert.Len(t, orch.bulkData[id], 0, "id: %v", id)
			}
			assert.Len(t, asyncChan, 0)
		})
		fmt.Println("done")
	}

}

func getMockOrchestrator() *asyncOrchestrator {
	return &asyncOrchestrator{
		bulkData: map[string]chan *asyncMessage{
			"no-bulk-id-1": make(chan *asyncMessage, 100),
			"no-bulk-id-2": make(chan *asyncMessage, 1),
			"bulk-id-1":    make(chan *asyncMessage, 100),
			"bulk-id-2":    make(chan *asyncMessage, 100),
		},
		bulkConfig: map[string]*config{
			"no-bulk-id-1": {maxSize: 100, maxTimeoutInMs: 100},
			"no-bulk-id-2": {maxSize: 100, maxTimeoutInMs: 100},
			"bulk-id-1":    {maxSize: 100, maxTimeoutInMs: 100},
			"bulk-id-2":    {maxSize: 100, maxTimeoutInMs: 100},
		},
		messageHandlerMap: map[string]MessageHandler{
			"no-bulk-id-1": getMockMessageHandler("no-bulk-id-1", false),
			"no-bulk-id-2": getMockMessageHandler("no-bulk-id-2", false),
			"bulk-id-1":    getMockMessageHandler("bulk-id-1", true),
			"bulk-id-2":    getMockMessageHandler("bulk-id-2", true),
		},
	}
}

func getMockMessageHandler(id string, isBulk bool) MessageHandler {
	return &mockMessageHandler{
		id:            id,
		canDrop:       false,
		isBulkEnabled: isBulk,
		maxBulkSize:   100,
		maxTimeout:    100,
	}
}

type mockMessageHandler struct {
	id                string
	canDrop           bool
	isBulkEnabled     bool
	maxBulkSize       int64
	maxTimeout        int64
	sendNumCalled     int64
	sendBulkNumCalled int64
}

func (m *mockMessageHandler) GetID() string { return m.id }
func (m *mockMessageHandler) Send(ctx context.Context, loggingMap map[string]string) error {
	m.sendNumCalled += 1
	return nil
}
func (m *mockMessageHandler) CanDrop() bool       { return m.canDrop }
func (m *mockMessageHandler) IsBulkEnabled() bool { return m.isBulkEnabled }
func (m *mockMessageHandler) SendBulk(ctx context.Context, messages []interface{}) error {
	m.sendBulkNumCalled += 1
	return nil
}
func (m *mockMessageHandler) MaxBulkSize() int64        { return m.maxBulkSize }
func (m *mockMessageHandler) MaxBulkTimeoutInMS() int64 { return m.maxTimeout }
