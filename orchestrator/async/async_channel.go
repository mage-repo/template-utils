package async

import (
	"context"
	"fmt"
	"sync"
	"time"
	"gitlab.com/mage-repo/template-utils/logger"
)

type MessageHandler interface {
	GetID() string
	Send(ctx context.Context, loggingMap map[string]string) error
	CanDrop() bool

	IsBulkEnabled() bool
	SendBulk(ctx context.Context, messages []interface{}) error
	MaxBulkSize() int64
	MaxBulkTimeoutInMS() int64
}

type asyncOrchestrator struct {
	bulkData          map[string]chan *asyncMessage
	bulkConfig        map[string]*config
	messageHandlerMap map[string]MessageHandler
}

type config struct {
	maxSize        int64
	maxTimeoutInMs int64
}

var (
	sender *asyncOrchestrator
	once   sync.Once
)

type asyncMessage struct {
	id      string
	ctx     context.Context
	loggingMap map[string]string
}

var asyncChan = make(chan *asyncMessage, MAX_CHANNEL_SIZE)

func (a *asyncOrchestrator) Send(ctx context.Context, id string, loggingMap map[string]string) {
	if len(asyncChan) >= MAX_CHANNEL_SIZE {
		handler := a.messageHandlerMap[id]
		if handler == nil {
			return
		}
		if handler.CanDrop() {
			return
		}
	}

	asyncChan <- &asyncMessage{
		id:      id,
		loggingMap: loggingMap,
		ctx:     ctx,
	}

}

func (a *asyncOrchestrator) asyncReader() {
	for elem := range asyncChan {
		a.handle(elem)
	}
}

func (a *asyncOrchestrator) bulkAsyncReader() {
	for i, c := range a.bulkConfig {
		id := i
		conf := c
		go func() {

			var bulkMessages []*asyncMessage
			dataChan := a.bulkData[id]
			for {
				expire := time.After(time.Duration(conf.maxTimeoutInMs) * time.Millisecond)
				select {
				case value := <-dataChan:
					bulkMessages = append(bulkMessages, value)
					if len(bulkMessages) >= int(conf.maxSize) {
						a.FlushAll(id, bulkMessages)
						bulkMessages = nil
					}

				case <-expire:
					if len(bulkMessages) > 0 {
						a.FlushAll(id, bulkMessages)
						bulkMessages = nil
					}
				}
			}
		}()
	}
}

func (a *asyncOrchestrator) FlushAll(id string, messages []*asyncMessage) {
	handler := a.messageHandlerMap[id]
	if handler == nil {
		return
	}
	var msgs []interface{}
	var ctx context.Context
	for _, m := range messages {
		msgs = append(msgs, m.loggingMap)
		ctx = m.ctx
	}
	err := handler.SendBulk(ctx, msgs)
	if err != nil {
		logger.LogErr(context.Background(),fmt.Sprintf("error sending message bulk to id: %v err: %v", id, err))
	}
}

func (a *asyncOrchestrator) handle(message *asyncMessage) {
	if message == nil {
		return
	}
	handler := a.messageHandlerMap[message.id]
	if handler == nil {
		return
	}

	if handler.IsBulkEnabled() {
		bulkChannel := a.bulkData[message.id]
		bulkChannel <- message
	} else {
		fmt.Printf("handle %+v",message.loggingMap)
		err := handler.Send(message.ctx, message.loggingMap)
		if err != nil {
			logger.LogErr(context.Background(),fmt.Sprintf("error sending message to id: %v err: %v", message.id, err))
			
		}
	}

}

func getAsyncConfig(handlers []MessageHandler) (map[string]chan *asyncMessage, map[string]*config, map[string]MessageHandler) {
	conf := getBulkConfig(handlers)
	handlerMap := getHandlerMap(handlers)
	bulkMap := make(map[string]chan *asyncMessage)
	for id, conf := range conf {
		handler := handlerMap[id]
		if handler.IsBulkEnabled() {
			bulkMap[id] = make(chan *asyncMessage, conf.maxSize)
		}
	}
	return bulkMap, conf, handlerMap
}

func getHandlerMap(handlers []MessageHandler) map[string]MessageHandler {
	m := make(map[string]MessageHandler)
	for _, h := range handlers {
		m[h.GetID()] = h
	}
	return m
}

func getBulkConfig(handlers []MessageHandler) map[string]*config {
	m := make(map[string]*config)
	for _, h := range handlers {
		if h.IsBulkEnabled() {
			m[h.GetID()] = &config{maxSize: h.MaxBulkSize(), maxTimeoutInMs: h.MaxBulkTimeoutInMS()}
		}
	}
	return m
}

func Init(handlers []MessageHandler) {
	once.Do(func() {
		dataMap, bulkConfig, handlerMap := getAsyncConfig(handlers)
		sender = &asyncOrchestrator{
			bulkData:          dataMap,
			bulkConfig:        bulkConfig,
			messageHandlerMap: handlerMap,
		}
		sender.bulkAsyncReader()
		go sender.asyncReader()
	})
}

func Send(ctx context.Context, id string, loggingMap map[string]string) {
	sender.Send(ctx, id, loggingMap)
}
