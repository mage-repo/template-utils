package async


type HandlerCreator func() (MessageHandler, error)

func GetAsyncHandlers(handlerCreators []HandlerCreator) ([]MessageHandler, error) {
	var handlers []MessageHandler
	for _, f := range handlerCreators {
		h, err := f()
		if err != nil {
			return nil, err
		}
		handlers = append(handlers, h)
	}
	return handlers, nil
}

