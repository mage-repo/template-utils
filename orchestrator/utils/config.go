package utils

import (
	"io/ioutil"
	"os"
	"gitlab.com/mage-repo/template-utils/errors"
	yaml "gopkg.in/yaml.v2"
	"fmt"
)

type Config struct {

	Methods     []Method `yaml:"methods"`
	ServiceName string   `yaml:"serviceName"`
}

type Method struct {
	Name                string          `yaml:"name"`
	MethodProperty    MethodProperty  `yaml:"methodProperties"`
	
}

type MethodProperty struct {
	RequestMiddlewareConfigs  []RequestMiddlewareConfig  `yaml:"requestMiddlewareConfig"`
	ResponseMiddlewareConfigs []ResponseMiddlewareConfig `yaml:"responseMiddlewareConfig"`
	MappedComponentName       string                     `yaml:"mappedComponentName"`
	MappedMethodName          string                     `yaml:"mappedMethodName"`

}

type RequestMiddlewareConfig struct {
	Name string `yaml:"name"`
	Info string `yaml:"info"`
	Type string `yaml:"type"`
	Checked bool `yaml:"checked"`
	Connection CallMethod `yaml:"connection"`

}

type ResponseMiddlewareConfig struct {
	Name string `yaml:"name"`
	Info string `yaml:"info"`
	Type string `yaml:"type"`
	Checked bool `yaml:"checked"`
	Connection CallMethod `yaml:"connection"`

}

type CallMethod struct {
	Name string `yaml:"name"`
}

var MiddlewareMap map[string]string
func GetMiddlewareArr (conf *Config) {
	MiddlewareMap = make(map[string]string)
	for _,method := range conf.Methods{
		for _,reqMiddleware := range method.MethodProperty.RequestMiddlewareConfigs{

			if _, ok := MiddlewareMap[reqMiddleware.Name]; !ok {
				MiddlewareMap[reqMiddleware.Name] = reqMiddleware.Connection.Name
			}
		}
	}
}


func GetConfig(configPath string) (*Config, error) {
	var conf Config
	yamlFile, err := ioutil.ReadFile(configPath)
	if err != nil{
		return nil, errors.NewInvalidArgumentsError(fmt.Sprintf("%s", err))
	}
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil{
		return nil, errors.NewInternalError(fmt.Sprintf("%s", err))
	}
	GetMiddlewareArr(&conf)
	return &conf, nil
}

func WriteConfig(conf *Config, path string) error {
	f, err := os.Create(path)
	if err != nil{
		return errors.NewInvalidArgumentsError(fmt.Sprintf("%s", err))
	}
	yamlData, err := yaml.Marshal(conf)
	if err != nil{
		return errors.NewInternalError(fmt.Sprintf("%s", err))
	}
	f.Write(yamlData)
	return nil
}
