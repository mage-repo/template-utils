package utils

import (
	"gitlab.com/mage-repo/template-utils/errors"

	"fmt"

	"google.golang.org/grpc"
)

var connMap map[string]*grpc.ClientConn

func SetUp(config *Config) error{

	connMap = make(map[string]*grpc.ClientConn)
	for _,method := range(config.Methods){
		
		var conn *grpc.ClientConn
		conn, err := grpc.Dial(fmt.Sprintf("%s:8080",method.MethodProperty.MappedComponentName), grpc.WithInsecure())
		//conn, err := grpc.Dial("localhost:8001", grpc.WithInsecure())
		if (err != nil) {
			return errors.NewInternalError(fmt.Sprintf("%s", err))
		}
		if _, ok := connMap[method.MethodProperty.MappedComponentName]; !ok {
			connMap[method.MethodProperty.MappedComponentName] = conn
		}
	}

	return nil
}

// GetDB helps you to get a connection
func GetApiServiceClientMap() map[string]*grpc.ClientConn {
	return connMap
}