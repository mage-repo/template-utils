package utils

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/mage-repo/template-utils/errors"
	"gitlab.com/mage-repo/template-utils/logger"
)

type InputQueryType struct {
	Fields []string
	Query  string
}

func generateAutoCompleteQuery(inputQuery InputQueryType) map[string]interface{} {
	generatedQuery := map[string]interface{}{
		"query": map[string]interface{}{
			"multi_match": map[string]interface{}{
				"fields": inputQuery.Fields,
				"query":  inputQuery.Query,
				"type":   "phrase_prefix",
			},
		},
	}
	return generatedQuery
}

func AutoComplete(ctx context.Context, inputQuery InputQueryType, index string) (string, error) {
	es := GetClient()
	query := generateAutoCompleteQuery(inputQuery)
	var errResponse string

	var r map[string]interface{}
	var buf bytes.Buffer

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		errResponse = fmt.Sprintf("Error encoding query: %s", err)

		logger.LogErr(ctx, errResponse)
		return "", errors.NewInvalidArgumentsError(errResponse)
	}
	// Perform the search request.
	res, err := es.Search(
		es.Search.WithContext(ctx),
		es.Search.WithIndex(index),
		es.Search.WithBody(&buf),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithPretty(),
	)
	if err != nil {
		errResponse = fmt.Sprintf("Error getting response: %s", err)

		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)
	}

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			errResponse = fmt.Sprintf("Error parsing the response body: %s", err)
			logger.LogErr(ctx, errResponse)
			return "", errors.NewInternalError(errResponse)

		} else {
			// Print the response status and error information.
			errResponse =
				fmt.Sprintf("[%s] %s: %s",
					res.Status(),
					e["error"].(map[string]interface{})["type"],
					e["error"].(map[string]interface{})["reason"],
				)

			logger.LogErr(ctx, errResponse)
			return "", errors.NewInternalError(errResponse)
		}
	}

	defer res.Body.Close()

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		errResponse = fmt.Sprintf("Error parsing the response body: %s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)
	}

	jsonForm, err := json.Marshal(r["hits"])

	if err != nil {
		errResponse = fmt.Sprintf("%s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError("errResponse")
	}

	return string(jsonForm), nil
}
