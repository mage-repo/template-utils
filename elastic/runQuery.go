package utils

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/mage-repo/template-utils/errors"
	"gitlab.com/mage-repo/template-utils/logger"
)

type matchType map[string]map[string]string
type shouldType []matchType
type boolType map[string]shouldType
type queryType map[string]boolType

func generateQuery(ctx context.Context, inputQuery map[string]string) map[string]queryType {

	var should shouldType

	for key, element := range inputQuery {
		should = append(should,
			matchType{"match": map[string]string{key: element}},
		)
	}

	boolObject := boolType{
		"should": should,
	}

	query := queryType{
		"bool": boolObject,
	}

	request := map[string]queryType{"query": query}

	// jsonForm, _ := json.MarshalIndent(request, "", " ")

	// logger.LogInfo(ctx, string(jsonForm))

	return request
}

func RunElasticQuery(ctx context.Context, inputQuery map[string]string, index string) (string, error) {

	var r map[string]interface{}
	var buf bytes.Buffer
	var errResponse string

	es := GetClient()

	request := generateQuery(ctx, inputQuery)

	if err := json.NewEncoder(&buf).Encode(request); err != nil {

		errResponse = fmt.Sprintf("Error encoding query: %s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)

	}

	// Perform the search request.
	res, err := es.Search(
		es.Search.WithContext(ctx),
		es.Search.WithIndex(index),
		es.Search.WithBody(&buf),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithPretty(),
	)
	if err != nil {
		errResponse = fmt.Sprintf("Error getting response: %s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			errResponse = fmt.Sprintf("Error parsing the response body: %s", err)
			logger.LogErr(ctx, errResponse)
			return "", errors.NewInternalError(errResponse)
		} else {
			// Print the response status and error information.
			errResponse =
				fmt.Sprintf("[%s] %s: %s",
					res.Status(),
					e["error"].(map[string]interface{})["type"],
					e["error"].(map[string]interface{})["reason"],
				)
			logger.LogErr(ctx, errResponse)
			return "", errors.NewInternalError(errResponse)
		}
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		errResponse = fmt.Sprintf("Error parsing the response body: %s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)
	}

	jsonForm, err := json.Marshal(r["hits"])

	if err != nil {
		errResponse = fmt.Sprintf("%s", err)
		logger.LogErr(ctx, errResponse)
		return "", errors.NewInternalError(errResponse)
	}

	return string(jsonForm), nil
}
