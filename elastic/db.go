package utils

import (
	"context"
	"fmt"

	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/mage-repo/template-utils/errors"
	"gitlab.com/mage-repo/template-utils/logger"
)

var es *elasticsearch.Client

func GetClient() *elasticsearch.Client {
	return es
}

func Init(ctx context.Context, CloudID string, Username string, Password string) error {
	logger.LogInfo(ctx, "Conneting to db")

	cfg := elasticsearch.Config{
		CloudID:  CloudID,
		Username: Username,
		Password: Password,
	}

	var err error
	var errResponse string

	es, err = elasticsearch.NewClient(cfg)

	if err != nil {
		errResponse = fmt.Sprintf("Error creating client: %v", err)
		logger.LogErr(ctx, errResponse)
		return errors.NewInternalError(errResponse)
	}

	res, err := es.Info()
	if err != nil {
		errResponse = fmt.Sprintf("Error getting response: %s", err)
		logger.LogErr(ctx, errResponse)
		return errors.NewInternalError(fmt.Sprintf("Error getting response: %s", err))
	}

	defer res.Body.Close()

	logger.LogInfo(ctx, "Connected to elastic")

	return nil

}
