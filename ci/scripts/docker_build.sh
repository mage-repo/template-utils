#! /bin/bash

set -e
CUR_DIR=`pwd`

for dir in */
do
  cd "$dir"
  if ls app
  then
    cd app
    APP_DIR=`pwd`
    for serv in */
    do
      cd "$serv"
      ls Dockerfile && docker build .
      cd "$APP_DIR"
    done
  fi
  cd "$CUR_DIR"
done