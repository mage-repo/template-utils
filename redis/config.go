package redis

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	UUID     string `yaml:"uuid"`
	Name     string `yaml:"name"`
	Type     string `yaml:"type"`
	URL      string `yaml:"url"`
	Port     string `yaml:"port"`
	Password string `yaml:"password"`
}

type Method struct {
	Name           string          `yaml:"name"`
	Type           string          `yaml:"type"`
	KeyIdentifiers []KeyIdentifier `yaml:"keyIdentifiers"`
	MaximumCount   string          `yaml:"maximumCount"`
	EvictionPolicy string          `yaml:"evictionPolicy"`
	Hash           string          `yaml:"hash"`
}

type KeyIdentifier struct {
	Name string `yaml:"name"`
	Type string `yaml:"type"`
}

func Read(path string) (*Config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := &Config{}

	err = yaml.Unmarshal(data, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
