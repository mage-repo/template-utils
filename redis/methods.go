package redis

import (
	"fmt"

	"github.com/go-redis/redis/v7"
	"gitlab.com/mage-repo/template-utils/errors"
)

type Service interface {
	Set(request string, keys []string) (string, error)
	Get(request string, keys []string) (string, error)
}

type redisService struct {
	client *redis.Client
}

func CreateRedisService(scc Config) (*redisService, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", scc.URL, scc.Port),
		Password: scc.Password,
		DB:       0,
	})
	_, err := client.Ping().Result()
	if err != nil {
		return nil, errors.NewInternalError(fmt.Sprintf("%v", err))
	}
	return &redisService{client: client}, nil
}

func (r *redisService) Set(request string, keys []string) (string, error) {
	key := MakeKey(request, keys)
	_, err := r.client.Set(key, request, 0).Result()
	if err != nil {
		return "", errors.NewInternalError(fmt.Sprintf("%v", err))
	}
	return key, nil
}

func (r *redisService) Get(request string, keys []string) (string, error) {
	key := MakeKey(request, keys)
	value, err := r.client.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			return "", errors.NewDataNotFoundError(fmt.Sprintf("%v", err))
		}
		return "", errors.NewInternalError(fmt.Sprintf("%v", err))
	}
	return value, nil
}
