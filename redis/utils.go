package redis

import (
	"strings"

	"github.com/tidwall/gjson"
)

func GetKeys(keyIdentifiers []KeyIdentifier) []string {
	keys := []string{}
	for i := range keyIdentifiers {
		keys = append(keys, keyIdentifiers[i].Name)
	}
	return keys
}

func MakeKey(request string, keys []string) string {
	var values []string
	for _, k := range keys {
		values = append(values, gjson.Get(request, k).String())
	}
	key := strings.Join(values, "|")
	return key
}
