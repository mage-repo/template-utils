package utils

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/mage-repo/template-utils/logger"
)

func GetReplacedString(tempo string, UserData map[string]interface{}) string {
	for k, i := range UserData {
		switch variable := i.(type) {
		case int32:
			tempo = strings.ReplaceAll(tempo, "[["+k+"]]", strconv.Itoa(int(i.(int32))))
		case int64:
			tempo = strings.ReplaceAll(tempo, "[["+k+"]]", strconv.Itoa(int(i.(int64))))
		case int:
			tempo = strings.ReplaceAll(tempo, "[["+k+"]]", strconv.Itoa(i.(int)))
		case string:
			tempo = strings.ReplaceAll(tempo, "[["+k+"]]", i.(string))
		default:
			logger.LogInfo(context.Background(), fmt.Sprintf("%v", variable))
		}
	}
	return tempo
}

func Response(baseURL string, endpoint string, header string, body string, Method string) (reponse string, err error) {
	URL := baseURL + endpoint
	client := &http.Client{}

	req, err := http.NewRequest(Method, URL, strings.NewReader(body))
	if err != nil {
		logger.LogErr(context.Background(), fmt.Sprintf("%v", err))
	}
	req.Header.Add("header", header)
	resp, err := client.Do(req)
	if err != nil {
		logger.LogErr(context.Background(), fmt.Sprintf("%v", err))
		return "", err
	}

	bodyJSON, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		logger.LogErr(context.Background(), fmt.Sprintf("%v", err))
		return "", err
	}
	return string(bodyJSON), err
}
