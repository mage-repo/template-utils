package utils

import (
	"fmt"
	"strings"
)


func TransfromWhereClause(method Method, valueMap map[string]interface{}) (string, []interface{}){

	var queryStr string
	var fieldsArr []string
	var args []interface{}


	for _, val := range method.Requests{

		if (val.ClauseType == "Where"){

			ele := val.Name[5:] + " " + Operations[val.OperationType] + " " + "? "
			fieldsArr = append(fieldsArr, ele)
			args = append(args, valueMap[val.Name])
		}
	}
	
	queryStr = strings.Join(fieldsArr, " AND ")
	fmt.Println(queryStr)
	
	return queryStr, args
}