package utils

import (
	"fmt"

	"gitlab.com/mage-repo/template-utils/errors"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

/*DB is connected database object*/
var DB *gorm.DB

func Setup(conf *Config) error {

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", conf.Service.URL, conf.Service.Port, conf.Service.Username, conf.Service.Password, "usermeta")

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		return errors.NewInternalError(fmt.Sprintf("%v", err))
	}

	//db.AutoMigrate(&Product{})
	DB = db
	return nil
}

// GetDB helps you to get a connection
func GetDB() *gorm.DB {
	return DB
}
