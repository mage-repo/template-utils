package utils

import (
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

var Operations = map[string]string{

	"CONTAINS": "in",
	"lt":       "<",
	"gt":       ">",
	"leq":      "<=",
	"geq":      ">=",
	"EQUALS":   "=",
}

// Port and dbname add here

type Config struct {
	Service struct {
		UUID     string `yaml:"uuid"`
		Name     string `yaml:"name"`
		Type     string `yaml:"type"`
		URL      string `yaml:"url"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Port     string `yaml:"port"`
	} `yaml:"service"`
	Methods []Method `yaml:"methods"`
}

type Method struct {
	Name         string    `yaml:"name"`
	QueryType    string    `yaml:"queryType"`
	Query        string    `yaml:"query"`
	SchemaType   string    `yaml:"schemaType"`
	SchemaFields []Fields  `yaml:"schemaFields"`
	Requests     []Request `yaml:"request"`
	Response     Response  `yaml:"response"`
}

type Fields struct {
	Name          string `yaml:"name"`
	Type          string `yaml:"type"`
	PrimaryKey    bool   `yaml:"primarykey"`
	AutoIncrement bool   `yaml:"autoIncrement"`
	Nullable      bool   `yaml:"nullable"`
}

type Request struct {
	Name          string `yaml:"name"`
	OperationType string `yaml:"type"`
	ClauseType    string `yaml:"clauseType"`
}

type Response struct {
	IsMultiReturn bool   `yaml:"isMultiReturn"`
	ReturnType    string `yaml:"returnType"`
}

func GetConfig(configPath string) *Config {
	var config Config
	yamlFile, _ := ioutil.ReadFile(configPath)
	_ = yaml.Unmarshal(yamlFile, &config)
	return &config
}

func WriteConfig(config *Config, path string) {
	f, _ := os.Create(path)
	yamlData, _ := yaml.Marshal(config.Service)
	f.Write(yamlData)
}
