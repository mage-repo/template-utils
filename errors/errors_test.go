package errors

import (
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestFromError(t *testing.T) {
	tests := []struct {
		name     string
		input    error
		expected *MageError
	}{
		{
			name:     "basic scenario",
			input:    status.New(codes.Internal, NewMageInternalError("sample error").Error()).Err(),
			expected: NewMageInternalError("sample error"),
		},
		{
			name:     "test scenario with non mage error",
			input:    status.New(codes.Internal, "sample error str").Err(),
			expected: NewMageInternalError("sample error str"),
		},
		{
			name:     "test scenario with mage error",
			input:    NewInternalError("sample internal error str"),
			expected: NewMageInternalError("sample internal error str"),
		},
	}

	for _, tt := range tests {
		test := tt
		t.Run(tt.name, func(t *testing.T) {
			err := FromError(test.input)
			if err == nil {
				if test.expected != nil {
					panic(fmt.Sprintf("unequal data, null, got: %v expected: %v", err, test.expected))
				} else { return }
			}
			if err.Message != test.expected.Message || err.Code != test.expected.Code {
				panic(fmt.Sprintf("unequal data got: %v expected: %v", err, test.expected))
			}
		})
	}
}
