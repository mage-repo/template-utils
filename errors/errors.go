package errors

import (
	"encoding/json"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ErrCode int64

const (
	// 40000 - 49999 are user input errors
	ErrCodeInvalidArguments     ErrCode = 40000
	ErrCodeUnauthenticatedToken ErrCode = 43000
	// 50000 - 59999 are internal application errors
	ErrCodeInternal ErrCode = 50000

	// 60000 - 69999 are data not found errors
	ErrCodeDataNotFound ErrCode = 60000
)

type IMageError interface {
	Error() string
	Status() status.Status
}

type MageError struct {
	Message string  `json:"message"`
	Code    ErrCode `json:"code"`
}

func (e *MageError) Status() ErrCode {
	return e.Code
}

func (e *MageError) Error() string {
	if e == nil {
		return ""
	}
	s, _ := json.Marshal(e)
	return string(s)
}

func NewInternalError(message string) error {
	msgJson := NewMageInternalError(message)
	return status.New(codes.Internal, msgJson.Error()).Err()
}

func NewMageInternalError(message string) *MageError {
	return &MageError{Message: message, Code: ErrCodeInternal}
}

func NewInvalidArgumentsError(message string) error {
	msgJson := NewMageInvalidArgumentsError(message)
	return status.New(codes.InvalidArgument, msgJson.Error()).Err()
}

func NewMageInvalidArgumentsError(message string) *MageError {
	return &MageError{Message: message, Code: ErrCodeInvalidArguments}
}

func NewDataNotFoundError(message string) error {
	msgJson := NewMageDataNotFound(message)
	return status.New(codes.NotFound, msgJson.Error()).Err()
}

func NewMageDataNotFound(message string) *MageError {
	return &MageError{Message: message, Code: ErrCodeDataNotFound}
}

func NewUnauthenticatedTokenError(message string) error {
	msgJson := NewMageUnauthenticatedTokenError(message)
	return status.New(codes.Unauthenticated, msgJson.Error()).Err()
}

func NewMageUnauthenticatedTokenError(message string) *MageError {
	return &MageError{Message: message, Code: ErrCodeUnauthenticatedToken}
}

func FromError(err error) *MageError {
	if err == nil {
		return nil
	}
	if errStatus, ok := status.FromError(err); !ok {
		return &MageError{Message: err.Error(), Code: ErrCodeInternal}
	} else {

		msg := errStatus.Message()
		if msg == "" {
			return nil
		}

		var mageError MageError
		if unmarshalErr := json.Unmarshal([]byte(msg), &mageError); unmarshalErr == nil {
			return &mageError
		}
		return NewMageInternalError(msg)
	}
}
