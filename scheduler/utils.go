package utils

import (
	"strings"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/robfig/cron/v3"
)

var crons []*cron.Cron
var cronName = make(map[string]*cron.Cron)
var s = gocron.NewScheduler(time.UTC)

type Runner interface {
	Schedule() string
	Run() error
	Name() string
}

func StartAll() {
	for _, c := range crons {
		c.Start()
	}
	s.StartAsync()
}

func StopAll() {
	for _, c := range crons {
		c.Stop()
	}
}

func RegisterJobs(RunableList []Runner) []*cron.Cron {
	functionList := RunableList
	for _, cr := range functionList {
		c := cron.New()
		c.AddFunc(cr.Schedule(), func() { cr.Run() })
		crons = append(crons, c)
		cronName[cr.Name()] = c
	}
	return crons
}

func AddJob(job Runner) *cron.Cron {
	c := cron.New()
	c.AddFunc(job.Schedule(), func() { job.Run() })
	crons = append(crons, c)
	cronName[job.Name()] = c
	return c
}

func StartJob(jobName string) {
	job := cronName[jobName]
	job.Start()
	s.StartAsync()
}

func StopJob(jobName string) {
	job := cronName[jobName]
	job.Stop()
}

func SetStatus(jobName string, status string) {
	if strings.ToLower(status) == `stop` {
		StopJob(jobName)
	} else if strings.ToLower(status) == `start` {
		StartJob(jobName)
	}
}
